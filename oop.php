<?php

trait Hewan {
	public $nama;
	public $darah = 50;
	public $jumlahKaki;
	public $keahlian;

	public function atraksi()
	{
		return $this->nama . " sedang " . $this->keahlian;
	}
}

trait Fight {

	use Hewan;

	public $attackPower;
	public $defencePower;

	public function serang($serang)
	{
		return $this->nama . " sedang menyerang " . $serang->nama;
	}

	public function diserang($serang)
	{
		$this->darah = $this->darah - ($serang->attackPower/$this->defencePower);
		return $this->nama . " sedang diserang " . $serang->nama;
		
		
	}
}

class Elang {

	use Fight;

	public function __construct($name)
	{
		$this->nama = $name;
		$this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
	}

	public function getInfoHewan()
	{
		$info = "Jenis hewan : " . __CLASS__ . "<br>" .
				"Nama : " . $this->nama . "<br>" .
				"Jumlah darah : " . $this->darah . "<br>" .
				"Jumlah kaki : " . $this->jumlahKaki . "<br>" .
				"Keahlian : " . $this->keahlian . "<br>" .
				"Attack Power : " . $this->attackPower . "<br>" .
				"Defence Power : " . $this->defencePower . "<br>";
		return $info;
	}

}

class Harimau {
	
	use Fight;

	public function __construct($name)
	{
		$this->nama = $name;
		$this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;

	}

	public function getInfoHewan()
	{
		$info = "Jenis hewan : " . __CLASS__ . "<br>" .
				"Nama : " . $this->nama . "<br>" .
				"Jumlah darah : " . $this->darah . "<br>" .
				"Jumlah kaki : " . $this->jumlahKaki . "<br>" .
				"Keahlian : " . $this->keahlian . "<br>" .
				"Attack Power : " . $this->attackPower . "<br>" .
				"Defence Power : " . $this->defencePower . "<br>";
		return $info;
	}
}



$elang = new Elang("elang_1");
$harimau = new Harimau("harimau_1");

echo $elang->getInfoHewan() . "<br>";
echo $harimau->getInfoHewan() . "<br>";
echo $elang->atraksi() . "<br>";
echo $harimau->atraksi() . "<br>";
echo "<br>";

echo "-----------------------------------<br>";
echo $harimau->serang($elang) . "<br>";
echo $elang->diserang($harimau) . "<br>";
echo "<br>";
echo $elang->getInfoHewan() . "<br>";

echo "-----------------------------------<br>";
echo $harimau->serang($elang) . "<br>";
echo $elang->diserang($harimau) . "<br>";
echo "<br>";
echo $elang->getInfoHewan() . "<br>";

echo "-----------------------------------<br>";
echo $elang->serang($harimau) . "<br>";
echo $harimau->diserang($elang) . "<br>";
echo "<br>";
echo $harimau->getInfoHewan() . "<br>";

echo "---------hasil pertarungan--------<br>";
echo $elang->getInfoHewan() . "<br>";
echo $harimau->getInfoHewan() . "<br>";


?>